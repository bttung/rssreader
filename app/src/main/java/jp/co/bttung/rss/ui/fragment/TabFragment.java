package jp.co.bttung.rss.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.co.bttung.rss.R;
import jp.co.bttung.rss.model.Entry;
import jp.co.bttung.rss.model.RssCategory;
import jp.co.bttung.rss.storage.DataManager;
import jp.co.bttung.rss.ui.adapter.RssRecyclerViewAdapter;
import jp.co.bttung.rss.ui.loader.RssAsyncTaskLoader;

/**
 * Created by a13561 on 2015/07/21.
 */
public class TabFragment extends ListFragment
        implements LoaderManager.LoaderCallbacks<List<Entry>> {

    private static String TAB_URL_KEY = "url";

    private Bundle mBundle;
    private List<Entry> mEntries;

    public static TabFragment newInstance(int position, RssCategory category) {
        Bundle bundle = new Bundle();
        bundle.putString(Entry.COLUMN_CATEGORY, category.getKey());
        bundle.putString(TAB_URL_KEY, category.getUrl());

        TabFragment fragment = new TabFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_content, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mBundle = getArguments();
        initLoader();
    }

    private void initLoader() {
        Loader loader = getLoaderManager().getLoader(0);
        if ( loader != null && loader.isReset() ) {
            getLoaderManager().restartLoader(0, mBundle, this);
        } else {
            getLoaderManager().initLoader(0, mBundle, this);
        }
    }

    @Bind(R.id.swipeContainer) SwipeRefreshLayout swipeContainer;
    @Bind(R.id.rss_progress_bar) ProgressBar progressBar;
    @Bind(R.id.rss_recycle_view) RecyclerView recyclerView;
    private RssRecyclerViewAdapter mRecyclerAdapter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        ButterKnife.bind(this, view);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initLoader();
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        mRecyclerAdapter = new RssRecyclerViewAdapter(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        recyclerView.setAdapter(mRecyclerAdapter);
    }

    @Override
    public Loader<List<Entry>> onCreateLoader(int i, Bundle bundle) {
        String category = bundle.getString(Entry.COLUMN_CATEGORY);
        String url = bundle.getString(TAB_URL_KEY);
        RssAsyncTaskLoader loader = new RssAsyncTaskLoader(getActivity(), category, url);
        loader.forceLoad();
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<List<Entry>> loader, List<Entry> entries) {
        progressBar.setVisibility(View.GONE);
        swipeContainer.setRefreshing(false);
        mEntries = entries;
        mRecyclerAdapter.setData(entries);
        return;
    }

    @Override
    public void onPause() {
        saveDatainBackground();
        super.onPause();
    }

    private void saveDatainBackground() {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        DataManager.saveData(mEntries);
                    }
                });
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }

    @Override
    public void onLoaderReset(Loader<List<Entry>> loader) {
        mRecyclerAdapter.setData(null);
    }
}