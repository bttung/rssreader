package jp.co.bttung.rss.ui.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import jp.co.bttung.rss.R;
import jp.co.bttung.rss.ui.activity.MainActivity;
import jp.co.bttung.rss.ui.activity.RSSFetchupTimeActivity;

/**
 * Created by a13561 on 2015/07/31.
 */
public class RSSBroadcastReceiver extends BroadcastReceiver {

    private static int TIMER_MAX_ERROR = 10; // 10 minutes
    private static boolean DEBUG = false;

    @Override
    public void onReceive(Context context, Intent received) {
        if ((received.getAction() != null) &&
                (received.getAction().equals("android.intent.action.BOOT_COMPLETED"))) {
            reAwakeTimerAfterReboot(context);
            return;
        }

        issueNotificaton(context, received);
    }

    private void reAwakeTimerAfterReboot(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(RSSNotification.TIMER, Context.MODE_PRIVATE);
        if (preferences.getBoolean(RSSFetchupTimeActivity.CHECKBOX_1, false)) {
            String timer1 = preferences.getString(RSSFetchupTimeActivity.ALARM_1, "");
            if (timer1.length() > 0) {
                RSSNotification.initAlarm(context, RSSFetchupTimeActivity.ALARM_1_ID, timer1);
            }
        }
        if (preferences.getBoolean(RSSFetchupTimeActivity.CHECKBOX_2, false)) {
            String timer1 = preferences.getString(RSSFetchupTimeActivity.ALARM_2, "");
            if (timer1.length() > 0) {
                RSSNotification.initAlarm(context, RSSFetchupTimeActivity.ALARM_2_ID, timer1);
            }
        }
    }

    private void issueNotificaton(Context context, Intent received) {
        int notifyId = received.getIntExtra(RSSNotification.NOTIFICATION_ID, 0);
        String timer = received.getStringExtra(RSSNotification.TIMER);
        int hour = received.getIntExtra(RSSNotification.HOUR, 0);
        int minute = received.getIntExtra(RSSNotification.MINUTE, 0);

        Calendar now = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.add(Calendar.MINUTE, TIMER_MAX_ERROR);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String setTime = formatter.format(calendar.getTime());
        String nowTime = formatter.format(now.getTime());

        if (DEBUG) {
            if (calendar.before(now)) {
                Toast.makeText(context, "Setted Time: " + setTime + ", now: " + nowTime, Toast.LENGTH_LONG).show();
                return;
            }
        }

        Resources resources = context.getResources();
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(context, notifyId, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setTicker(resources.getString(R.string.notify_ticker))
                .setWhen(System.currentTimeMillis())
                .setContentTitle(resources.getString(R.string.notify_title))
                .setContentText(resources.getString(R.string.notify_content))
                .setDefaults(Notification.DEFAULT_LIGHTS)
                .setDefaults(Notification.DEFAULT_VIBRATE)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        notificationManager.cancelAll();
        notificationManager.notify(R.string.app_name, notification);
    }
}