package jp.co.bttung.rss.ui.loader;

import android.content.Context;

import com.activeandroid.query.Select;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import jp.co.bttung.rss.model.Entry;
import jp.co.bttung.rss.parser.RSSParser;

/**
 * Created by a13561 on 2015/07/21.
 */
public class RssAsyncTaskLoader extends CustomAsyncTaskLoader<List<Entry>> {

    private static final String CACHE = "cache";
    private static final int CACHE_SIZE = 10 * 1024 * 1024; // 10MB

    private String mCategory;
    private String mUrl;
    private Context mContext;

    public RssAsyncTaskLoader(Context context, String category, String url) {
        super(context);
        mCategory = category;
        mUrl = url;
        mContext = context;
    }

    @Override
    public List<Entry> loadInBackground() {

        Request request = new Request.Builder()
                .url(mUrl)
                .get()
                .build();

        OkHttpClient client = new OkHttpClient();
        File cacheDir = new File(mContext.getCacheDir(), CACHE);
        Cache cache = new Cache(cacheDir, CACHE_SIZE);
        client.setCache(cache);

        List<Entry> entries = new Select().from(Entry.class)
                .where(Entry.COLUMN_CATEGORY + " = ?", mCategory)
//                .orderBy("id DESC")
                .execute();

        try {
            Response response = client.newCall(request).execute();
            InputStream input = response.body().byteStream();
            List<Entry> newEntries = RSSParser.parseXml(input);

            for (Entry entry : newEntries) {
                entry.setCategory(mCategory);
            }

            for (Entry entry : newEntries) {
                boolean saved = false;
                for (Entry savedEntry : entries) {
                    if (savedEntry.equals(entry)) {
                        saved = true;
                        break;
                    }
                }
                if (!saved) {
                    entries.add(entry);
                }
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Collections.reverse(entries);
        return entries;
    }
}