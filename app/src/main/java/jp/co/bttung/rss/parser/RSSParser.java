package jp.co.bttung.rss.parser;

import android.util.Xml;

import org.jsoup.Jsoup;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import jp.co.bttung.rss.model.Entry;

/**
 * Created by a13561 on 2015/07/24.
 */
public class RSSParser {

    public static String RSS_ITEM_TAG = "item";
    public static String RSS_ITEM_TITLE = "title";
    public static String RSS_ITEM_DESCRIPTION = "description";
    public static String RSS_ITEM_LINK = "link";
    public static String RSS_ITEM_CONTENT = "encoded";

    public static String getThumbImageSrc(String content) {
        try {
            org.jsoup.nodes.Document document = Jsoup.parse(content, "", Parser.xmlParser());
            Elements thumb = document.getElementsByClass("entry-image");
            if (thumb.size() == 0) {
                return null;
            }
            return thumb.first().attr("src");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // XMLをパースする
    public static List<Entry> parseXml(InputStream is) throws IOException, XmlPullParserException {

        List<Entry> entries = new ArrayList<>();
        XmlPullParser parser = Xml.newPullParser();

        try {
            parser.setInput(is, null);
            int eventType = parser.getEventType();
            Entry entry = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tag;
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        tag = parser.getName();
                        if (tag.equals(RSS_ITEM_TAG)) {
                            entry = new Entry();
                        } else if (entry != null) {
                            if (tag.equals(RSS_ITEM_TITLE)) {
                                entry.setTitle(parser.nextText());
                            } else if (tag.equals(RSS_ITEM_DESCRIPTION)) {
                                entry.setDescription(parser.nextText());
                            } else if (tag.equals(RSS_ITEM_LINK)) {
                                entry.setUrl(parser.nextText());
                            } else if (tag.equals(RSS_ITEM_CONTENT)) {
                                String imgSrc = getThumbImageSrc(parser.nextText());
                                entry.setThumbUrl(imgSrc);
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        tag = parser.getName();
                        if (tag.equals(RSS_ITEM_TAG)) {
                            entries.add(entry);
                        }
                        break;
                }
                eventType = parser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return entries;
    }
}
