package jp.co.bttung.rss.ui.activity;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.activeandroid.query.Select;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.co.bttung.rss.R;
import jp.co.bttung.rss.model.Entry;
import jp.co.bttung.rss.storage.DataManager;
import jp.co.bttung.rss.ui.adapter.TabPagerAdapter;
import jp.co.bttung.rss.ui.common.SlidingTabLayout;


public class MainActivity extends AppCompatActivity {

    @Bind(R.id.viewpager) ViewPager viewPager;
    @Bind(R.id.sliding_tabs) SlidingTabLayout slidingTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        TabPagerAdapter tabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(tabPagerAdapter);
        slidingTabLayout.setViewPager(viewPager);
        customSlidingTabColor();
    }

    private void customSlidingTabColor() {
        final TypedArray colors = getResources().obtainTypedArray(R.array.tab_color);

        slidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return colors.getColor(position % colors.length(), 0);
            }

            @Override
            public int getDividerColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete_cache) {
            deleteImageinBackground();
            return true;
        } else if (id == R.id.action_set_feed_time) {
            Intent intent = new Intent(getApplicationContext(), RSSFetchupTimeActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteImageinBackground() {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        deleteAllImageCache();
                    }
                });
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }

    private void deleteAllImageCache() {
        List<Entry> entries = new Select().from(Entry.class)
                .execute();
        DataManager.deleteThumbImage(entries);
    }
}
