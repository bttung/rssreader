package jp.co.bttung.rss.model;

/**
 * Created by a13561 on 2015/07/23.
 */
public class RssCategory {

    private String mKey;
    private String mTitle;
    private String mUrl;

    public String getKey() {
        return mKey;
    }

    public void setKey(String key) {
        mKey = key;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }
}
