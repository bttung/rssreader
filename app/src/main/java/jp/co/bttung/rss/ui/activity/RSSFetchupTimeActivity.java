package jp.co.bttung.rss.ui.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.co.bttung.rss.R;
import jp.co.bttung.rss.ui.fragment.TimePickerFragment;
import jp.co.bttung.rss.ui.notification.RSSNotification;

/**
 * Created by a13561 on 2015/07/31.
 */
public class RSSFetchupTimeActivity extends AppCompatActivity implements
        TimePickerFragment.OnCompleteListener {

    public static String CHECKBOX_1 = "checkbox_1";
    public static String CHECKBOX_2 = "checkbox_2";
    public static String ALARM_1 = "alarm_1";
    public static String ALARM_2 = "alarm_2";
    public static int ALARM_1_ID = 1;
    public static int ALARM_2_ID = 2;

    @Bind(R.id.fetch_time_checkBox1) CheckBox mCheckBox1;
    @Bind(R.id.fetch_time_checkBox2) CheckBox mCheckBox2;
    @Bind(R.id.fetch_time_tv1) TextView mFetchTimeTv1;
    @Bind(R.id.fetch_time_tv2) TextView mFetchTimeTv2;

    private SharedPreferences mPreferences;
    private View edittingView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fetchtime);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        ButterKnife.bind(this);

        mPreferences = getSharedPreferences(RSSNotification.TIMER, Context.MODE_PRIVATE);

        String timer1 = mPreferences.getString(ALARM_1, "");
        String timer2 = mPreferences.getString(ALARM_2, "");
        if (timer1.length() > 0) {
            mFetchTimeTv1.setText(timer1);
        }
        if (timer2.length() > 0) {
            mFetchTimeTv2.setText(timer2);
        }

        Boolean checkbox1 = mPreferences.getBoolean(CHECKBOX_1, false);
        Boolean checkbox2 = mPreferences.getBoolean(CHECKBOX_2, false);
        mCheckBox1.setChecked(checkbox1);
        mCheckBox2.setChecked(checkbox2);
    }

    @OnClick({R.id.fetch_time_tv1, R.id.fetch_time_tv2})
    public void onClick(View v) {
        edittingView = v;
        DialogFragment timePicker = new TimePickerFragment();
        timePicker.show(getSupportFragmentManager(), TimePickerFragment.TAG);
    }

    @Override
    public void onSelectedTime(int hourOfDay, int minute) {
        SharedPreferences.Editor editor = mPreferences.edit();

        if (edittingView.getId() == mFetchTimeTv1.getId()) {
            String timer1 = String.format("%02d:%02d", hourOfDay, minute);
            mFetchTimeTv1.setText(timer1);
            editor.putString(ALARM_1, timer1);
        } else {
            String timer2 = String.format("%02d:%02d", hourOfDay, minute);
            mFetchTimeTv2.setText(timer2);
            editor.putString(ALARM_2, timer2);
        }

        editor.apply();
    }

    @OnClick(R.id.fetch_time_yes)
    void updateTimer() {
        Context context = getApplicationContext();

        if (mCheckBox1.isChecked()) {
            RSSNotification.initAlarm(context, ALARM_1_ID, mFetchTimeTv1.getText().toString());
        } else {
            RSSNotification.cancelAlarm(context, ALARM_1_ID);
        }

        if (mCheckBox2.isChecked()) {
            RSSNotification.initAlarm(context, ALARM_2_ID, mFetchTimeTv2.getText().toString());
        } else {
            RSSNotification.cancelAlarm(context, ALARM_2_ID);
        }

        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putBoolean(CHECKBOX_1, mCheckBox1.isChecked());
        editor.putBoolean(CHECKBOX_2, mCheckBox2.isChecked());
        editor.apply();

        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
