package jp.co.bttung.rss.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import jp.co.bttung.rss.R;
import jp.co.bttung.rss.model.RssCategory;
import jp.co.bttung.rss.ui.fragment.TabFragment;

/**
 * Created by a13561 on 2015/07/21.
 */
public class TabPagerAdapter extends FragmentPagerAdapter {

    private Activity mActivity;
    private Context mContext;
    private RssCategory[] mRssCategories;

    public TabPagerAdapter(FragmentManager fm, Activity activity) {
        super(fm);

        mActivity = activity;
        mContext = mActivity.getApplicationContext();

        TypedArray rssTabs = mContext.getResources().obtainTypedArray(R.array.rss_content);
        mRssCategories = new RssCategory[rssTabs.length()];

        for (int i = 0; i < rssTabs.length(); i++) {
            int tabResourceId = rssTabs.getResourceId(i, 0);
            String[] values = mContext.getResources().getStringArray(tabResourceId);
            if (values.length > 2) {
                RssCategory category = new RssCategory();
                category.setKey(values[0]);
                category.setTitle(values[1]);
                category.setUrl(values[2]);
                mRssCategories[i] = category;
            }
        }

        rssTabs.recycle();
    }

    @Override
    public int getCount() {
        return mRssCategories.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mRssCategories[position].getTitle();
    }

    @Override
    public Fragment getItem(int position) {
        return TabFragment.newInstance(position, mRssCategories[position]);
    }
}