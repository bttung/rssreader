package jp.co.bttung.rss.storage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import jp.co.bttung.rss.debug.log.AppLog;

/**
 * Created by a13561 on 2015/07/29.
 */
public class ImageStorage {

    private static final String PATH = "/Image";
    private static final String EXTENSION = ".png";
    private static final int COMPRESS_RATE = 100;

    public static Bitmap loadBitmap(String filename) {
        if (filename == null) {
            return null;
        }
        Bitmap bitmap = BitmapFactory.decodeFile(filename);
        return bitmap;
    }

    public static String saveBitmap(Bitmap bitmap, String filename) {

        String sdCard = getSDCardPath();
        if (sdCard == null) {
            Log.e(AppLog.TAG, "Dont have sdcard");
            return null;
        }

        String path = sdCard + PATH;
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        String fullPath = path + "/" + filename + EXTENSION;
        File file = new File(fullPath);
        try {
            FileOutputStream output = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, COMPRESS_RATE, output);
            output.flush();
            output.close();
            Log.w(AppLog.TAG, "Saved " + fullPath);
            return fullPath;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            return file.delete();
        }
        // file doesnt exist = deleted
        return true;
    }

    public static String getSDCardPath() {
        File path = Environment.getExternalStorageDirectory();
        return path.getAbsolutePath();
    }
}
