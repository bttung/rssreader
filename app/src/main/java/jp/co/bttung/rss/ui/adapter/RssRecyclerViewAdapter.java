package jp.co.bttung.rss.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import jp.co.bttung.rss.R;
import jp.co.bttung.rss.model.Entry;
import jp.co.bttung.rss.model.EntryRecyclerViewHolder;
import jp.co.bttung.rss.storage.ImageStorage;
import jp.co.bttung.rss.ui.activity.EntryDetailActivity;

/**
 * Created by a13561 on 2015/07/30.
 */
public class RssRecyclerViewAdapter extends RecyclerView.Adapter<EntryRecyclerViewHolder> {

    private static int THUMB_WIDTH = 120;
    private static int THUMB_HEIGHT = 120;

    private List<Entry> mData;
    private Activity mActivity;
    private Context mContext;

    public RssRecyclerViewAdapter(Activity activity) {
        mActivity = activity;
        mContext = activity.getApplicationContext();
    }

    public void setData(List<Entry> data) {
        this.mData = data;
        notifyDataSetChanged();
    }

    @Override
    public EntryRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.entry_row, null);
        EntryRecyclerViewHolder viewHolder = new EntryRecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EntryRecyclerViewHolder entryRecyclerViewHolder, int i) {
        Entry entry = mData.get(i);
        entryRecyclerViewHolder.title.setText(entry.getTitle());

        entryRecyclerViewHolder.title.setOnClickListener(clickListener);
        entryRecyclerViewHolder.thumb.setOnClickListener(clickListener);

        entryRecyclerViewHolder.title.setTag(entryRecyclerViewHolder);
        entryRecyclerViewHolder.thumb.setTag(entryRecyclerViewHolder);

        loadThumb(entryRecyclerViewHolder, entry);
    }

    private void loadThumb(final EntryRecyclerViewHolder entryRecyclerViewHolder, final Entry entry) {
        if ((entry.getThumbUrl() == null)) {
            return;
        }

        Bitmap thumbImg = null;
        if (entry.getThumbPath() != null) {
            thumbImg = ImageStorage.loadBitmap(String.valueOf(entry.getThumbPath()));
        }

        // Load Image from Internet
        if (thumbImg == null)
        {
            Picasso.with(mContext)
                    .load(entry.getThumbUrl())
                    .error(R.drawable.ic_launcher)
                    .into(entryRecyclerViewHolder.thumb, new Callback() {
                        @Override
                        public void onSuccess() {
                            if (entry.getId() == null) {
                                return;
                            }
                            Bitmap thumb = ((BitmapDrawable) entryRecyclerViewHolder.thumb.getDrawable()).getBitmap();
                            entry.setThumbBitmap(thumb);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        } else {
            // Load from storage
            entryRecyclerViewHolder.thumb.setImageBitmap(thumbImg);
        }
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EntryRecyclerViewHolder holder = (EntryRecyclerViewHolder) view.getTag();
            int position = holder.getPosition();
            Entry entry = mData.get(position);

            Intent intent = new Intent(mContext, EntryDetailActivity.class);
            intent.putExtra(Entry.COLUMN_URL, entry.getUrl());
            intent.putExtra(Entry.COLUMN_TITLE, entry.getTitle());
            mActivity.startActivity(intent);
        }
    };

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }
}
