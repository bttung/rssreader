package jp.co.bttung.rss.storage;

import android.util.Log;

import java.util.List;

import jp.co.bttung.rss.debug.log.AppLog;
import jp.co.bttung.rss.model.Entry;

/**
 * Created by a13561 on 2015/07/30.
 */
public class DataManager {

    public static void saveData(List<Entry> entries) {

        if (entries == null) {
            return;
        }

        for (Entry entry : entries) {
            boolean saveFlag = false;

            if (entry.getId() != null) {
                if (entry.getThumbBitmap() != null) {
                    String thumbPath = ImageStorage.saveBitmap(entry.getThumbBitmap(),
                            String.valueOf(entry.getId()));
                    if (thumbPath != null) {
                        entry.setThumbPath(thumbPath);
                        Log.w(AppLog.TAG, thumbPath);
                    }
                }
                saveFlag = true;
            } else if (!entry.isSaved()){
                saveFlag = true;
            }

            if (saveFlag) {
                entry.setSaved(true);
                entry.save();
            }
        }
    }

    public static void deleteThumbImage(List<Entry> entries) {
        for (Entry entry : entries) {
            if (entry.getThumbPath() != null) {
                if (ImageStorage.deleteFile(entry.getThumbPath())) {
                    entry.setThumbPath(null);
                    entry.save();
                }
            }
        }
    }

//    public static boolean isEmulator() {
//        String model = Build.MODEL;
//        Log.d(AppLog.TAG, "model=" + model);
//        String product = Build.PRODUCT;
//        Log.d(AppLog.TAG, "product=" + product);
//        boolean isEmulator = false;
//        if (product != null) {
//            isEmulator = product.equals("sdk") || product.contains("_sdk") || product.contains("sdk_");
//        }
//        Log.d(AppLog.TAG, "isEmulator=" + isEmulator);
//        return isEmulator;
//    }
}
