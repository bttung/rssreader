package jp.co.bttung.rss.model;

import android.graphics.Bitmap;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;

/**
 * Created by a13561 on 2015/07/21.
 */
@Table(name = "Entry")
public class Entry extends com.activeandroid.Model implements Serializable {

    public static final String COLUMN_CATEGORY = "category";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_URL = "url";
    public static final String COLUMN_THUMB_URL = "thumb_url";
    public static final String COLUMN_THUMB_SAVE_PATH = "thumb_path";

    @Column(name = COLUMN_CATEGORY)
    private String mCategory;

    @Column(name = COLUMN_TITLE)
    private String mTitle;

    @Column(name = COLUMN_DESCRIPTION)
    private String mDescription;

    @Column(name = COLUMN_URL)
    private String mUrl;

    @Column(name = COLUMN_THUMB_URL)
    private String mThumbUrl;

    @Column(name = COLUMN_THUMB_SAVE_PATH)
    private String mThumbPath;

    private boolean mSaved;
    private Bitmap mThumbBitmap;

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Entry)) {
            return false;
        }

        Entry entry = (Entry) obj;
        return (this.mCategory.equals(entry.getCategory()) &&
                this.mUrl.equals(entry.getUrl()));
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getCategory() {
        return mCategory;
    }

    public void setCategory(String genreType) {
        mCategory = genreType;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public String getThumbUrl() {
        return mThumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        mThumbUrl = thumbUrl;
    }

    public String getThumbPath() {
        return mThumbPath;
    }

    public void setThumbPath(String thumbPath) {
        mThumbPath = thumbPath;
    }

    public Bitmap getThumbBitmap() {
        return mThumbBitmap;
    }

    public void setThumbBitmap(Bitmap thumbBitmap) {
        mThumbBitmap = thumbBitmap;
    }

    public boolean isSaved() {
        return mSaved;
    }

    public void setSaved(boolean saved) {
        mSaved = saved;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "mId=" + getId() +
                ", mCategory=" + mCategory +
                ", mTitle='" + mTitle + '\'' +
                '}';
    }
}
