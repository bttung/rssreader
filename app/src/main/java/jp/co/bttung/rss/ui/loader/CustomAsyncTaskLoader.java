package jp.co.bttung.rss.ui.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

/**
 * Created by a13561 on 2015/07/23.
 * onLoadFinished not called 問題対策
 * 参考URL: http://stackoverflow.com/questions/7474756/onloadfinished-not-called-after-coming-back-from-a-home-button-press
 */
public abstract class CustomAsyncTaskLoader<D> extends AsyncTaskLoader<D> {

    private D data;

    public CustomAsyncTaskLoader(Context context) {
        super(context);
    }

    @Override
    public void deliverResult(D data) {
        if (isReset()) {
            // An async query came in while the loader is stopped
            return;
        }

        this.data = data;

        super.deliverResult(data);
    }


    @Override
    protected void onStartLoading() {
        if (data != null) {
            deliverResult(data);
        }

        if (takeContentChanged() || data == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        // Attempt to cancel the current load task if possible.
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();

        // Ensure the loader is stopped
        onStopLoading();

        data = null;
    }
}