package jp.co.bttung.rss.model;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.co.bttung.rss.R;

/**
 * Created by a13561 on 2015/07/30.
 */
public class EntryRecyclerViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.entry_title) public TextView title;
    @Bind(R.id.entry_thumb_iv) public ImageView thumb;

    public EntryRecyclerViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}
