package jp.co.bttung.rss.ui.notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

/**
 * Created by a13561 on 2015/08/03.
 */
public class RSSNotification {

    public static String NOTIFICATION_ID = "notification_id";
    public static String TIMER = "timer";
    public static String HOUR = "hour";
    public static String MINUTE = "minute";

    public static void initAlarm(Context context, int notifyId, String time) {
        int hour = Integer.parseInt(time.substring(0, 2));
        int min = Integer.parseInt(time.substring(3, 5));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);

        Intent intent = new Intent(context, RSSBroadcastReceiver.class);
        intent.putExtra(NOTIFICATION_ID, notifyId);
        intent.putExtra(HOUR, hour);
        intent.putExtra(MINUTE, min);

        PendingIntent pending = PendingIntent.getBroadcast(context, notifyId, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pending);
    }

    public static void cancelAlarm(Context context, int notifyId)
    {
        Intent intent = new Intent(context, RSSBroadcastReceiver.class);
        PendingIntent pending =
                PendingIntent.getBroadcast(context, notifyId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pending);
    }
}
